# import uuid
# from time import sleep
# from string import Template
# from pathlib import Path

# from urllib.error import URLError
# # from aiobotocore.session import get_session
# from rdflib.plugins.stores import sparqlstore
# from rdflib import Graph
# import re
# import traceback

# import logging
# rmap_log = logging.getLogger('rmap_log')

# CODE_DIR = Path(__file__).resolve().parents[2]
# TEMPLATE_DIR = Path(CODE_DIR, 'sparql_templates')
# TEST_PATH_TEMPLATE_FOAF = Path(TEMPLATE_DIR, 'rdfgraph-graphdb_update_template-foaf_person.sparql')

# # dict_schema_mapper = {
# #     # 'schema_colors': rdflib_mappers.mapper_color,
# #     # 'schema_elt_material_mechanical_stone_fromproducer': rdflib_mappers.mapper_elt_material_mechanical_stone_fromproducer,
# #     # 'orphaned_turtle-avro': load_rdf_from_turtle_avro,
# #     # 'orphaned_image-avro': rdflib_mappers.orphaned_image_map,
# #     # 'geom_scrs_transformation-avro': rdflib_mappers.mapper_SCRS_trsf,
# #     # 'geom_as_place_scrs-avro': rdflib_mappers.mapper_geom_as_spsix,
# #     'dschema_avro_apofeat': map_apofeat,            # at_place_observed_feature
# #     'dschema_avro_apsfeat': map_apsfeat,            # at_place_simulated_feature   
# #     'dschema_avro_ubot': load_ttl_from_s3,          # utils_bootstrap_ontology_turtle       
# #     'dschema_avro_gaps': map_gaps,                  # geom_as_place_scrs
# #     'dschema_avro_gst' : map_gst,                   # geom_scrs_transformation
# #     'dschema_avro_gasngs' : map_gasngs,             # geom_as_subplace_new_geom_scrs
# #     'dschema_avro_gasiog' : map_gasiog,             # geom_as_subplace_indexed_on_geom
# #     'dschema_avro_matoap': map_matoap,              # material_observed_at_place
# #     'dschema_avro_omabcap': map_omabcap,            # ouvrage_minimal_as_building_component_and_place
# #     'dschema_avro_pasp' : map_pasp,                 # place_as_subplace
# #     'dschema_avro_slamt' : map_slamt                # simulation_lmgc90_as_minimal_tarball 
# # }teg

# async def context_processor_sparqlstore_update(sparqlstore : sparqlstore.SPARQLUpdateStore, 
#                                          named_graph : str, 
#                                          uri_root : str, 
#                                          path_queryfile : Path,
#                                          **kwargs):
#     try:
#         with open(path_queryfile,'r',encoding='utf-8') as file:
#             s = file.read()
#             uris = {}
#             uris_match = set(re.findall(pattern='<\$_uri(.*?)>', string=s)) # set(findall(pattern, string)) for distincts matches
#             for u_number in uris_match:
#                 urikey = f"_uri{u_number}"
#                 uris[urikey] = uri_root + str(uuid.uuid4())[:8]
#             logging.info(f"Created {len(uris_match)} URIs : {uris}")
            
#             supdate = Template(s).substitute(kwargs|uris)
#             logging.debug(f"--- --- SUBSTITUED TEMPLATE CONTENT --- ---\n{supdate}")
#             sparqlstore.update(supdate, queryGraph=named_graph)
#     except KeyError:
#         logging.error(f"Textual JSON data and specified SPARQL ingest schema does not match (context_processor_sparqlstore_update)")
#         logging.error(f"Please, chech missing value \n {Template(s).safe_substitute(kwargs|uris)}")
#     except FileNotFoundError as fnfe:
#         logging.error(fnfe)
#         logging.error(f"No SPARQL file found at path : {path_queryfile}")
#     except URLError as ue:
#         logging.error(ue)
#         logging.error(f"Check Sparql Store connection configuration and health")
#     return uris

# async def map_to_rdf(schema_name, data, uri_root, sparqlstore, named_graph_identifier):
#     # a fusionner avec context_processor... c'est un vieil héritage
#     # try:
#     path_queryfile = Path(TEMPLATE_DIR, str(schema_name+'.sparql'))
#     return await context_processor_sparqlstore_update(sparqlstore=sparqlstore, 
#                                     named_graph=named_graph_identifier, 
#                                     uri_root=uri_root, 
#                                     path_queryfile=path_queryfile,
#                                     **data)
        
        
#     # # except KeyError:
#     # #     print(f"{schema_name} is an invalid schema name, non existant in dict_schema_mapper")
#     # except Exception as e:
#     #     traceback.print_exc()

#     # return True