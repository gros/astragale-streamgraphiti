# *Stream-Graphiti*: schema-based 'transform&load' to your triple-store

`stream_graphiti` is a faust-based stream processor for RDF data mapping to a SPARQL Store compliant triple store. Its behavior is heavily conditionned to the provided schemas and mappers.

It features a dynamic URI generation, easing testing protocols and via flexible graph extension.

## Features
+ An [Apache Kafka](https://kafka.apache.org/) RDF sink, with many templates !



### Built with
+ [Faust](https://github.com/faust-streaming/faust): a real-time data pipeline processor adapting *Kafka Streams* to Python (based on FastAPI)


### For debug
Local run within *./21bb_service-streamgraphiti/service-streamgraphiti*: `faust -A app.main worker`

Build with dockerfile : `docker-compose up --force-recreate --build`

## Usage
`stream_graphiti` is intended to be used in a small-scale application, scalability can be reached through Faust and Kafka Connectors.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.


## Roadmap
**Goals for v0.5**
+ Rely on templated queries `INSERT` or `INSERT WHERE` instead of python gibberish.

**Goals for v0.3**
+ Add ability to interact with named graph. Add *root_uri* and *graph_name* (default=DATASET_DEFAULT_GRAPH_ID) to settings

**Goals for v0.4**
+ Add the possibility of static URI definition
+ Add external declaration of schema/destination/mappers to make the service more flexible (actually, force restart to update mapping behaviors).

## License
Open-source project, MIT License.

## Project Status
+ **(v0.3, 21/03/2023)** Different mappers integrated
+ **(v0.2, 12/11/2022)** Dockerized version of stream-graphiti